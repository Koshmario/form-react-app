import React, {Component} from 'react'
import {render} from 'react-dom'
import CheckIp from './components/CheckIp'
import ShowResult from './components/getResult'
import History from './components/History'
import './index.scss'
import api from "./http"

class App extends Component {
	state = {
		ip: '',
		response: null,
		history: [],
		idItem: 0
	}

	componentWillMount() {
		const value = localStorage.getItem('form_app_input_value')
		const history = localStorage.getItem('form_app_history')
		if (value) {
			this.setState({ip: value})
		}
		if (history) {
			this.setState({history: JSON.parse(history)})
		}
	}

	handleChangeInput = (event) => {
		this.setState({
			ip: event.target.value
		})
		localStorage.setItem('form_app_input_value', event.target.value)
	}

	getInfoForIP = () => {
		api.get('/', {
			params: {ip: this.state.ip}
		})
			.then((response) => {
				this.setState({
					response: response.data
				})
				if (typeof response.data === 'object') {
					response.data.time = new Date().toLocaleDateString('ru', {
						day: '2-digit',
						month: '2-digit',
						year: 'numeric',
						hour: '2-digit',
						minute: '2-digit',
						second: '2-digit'
					})
					response.data.id = new Date().getTime()

					this.setState({
						history: this.state.history.concat([response.data])
					})

					localStorage.setItem('form_app_history', JSON.stringify(this.state.history))
				}
			})
	}

	render() {
		return (
			<div className='form-app alert-secondary'>
				<div className="container">
					<div className="row">
						<div className="col-lg-7">
							<CheckIp
								ip={this.state.ip}
								handleChangeInput={this.handleChangeInput}
								getInfoForIP={this.getInfoForIP}
							/>
							<ShowResult response={this.state.response}/>
						</div>
						<div className="col-lg-5">
							<History history={this.state.history}/>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
render(<App/>, document.getElementById('root'))