import React from 'react'
import Success from './Success'
import Error from './Error'

function getResult(props){
	const response = props.response
	if(response) {
		if(typeof response === 'object') {
			return <Success response={response}  />
		} else if (typeof response === 'string') {
			return <Error />
		}
	} else {
		return null
	}
}

export default getResult