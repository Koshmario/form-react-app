import React from 'react'
import './Success.scss'

function Success(props) {
	const response = props.response
	return(
		<section className='list-container'>
			<ul className="list-group">
				<li className="list-group-item">Страна: {response.country_rus}</li>
				<li className="list-group-item">Регион: {response.region_rus}</li>
				<li className="list-group-item">Город: {response.city_rus}</li>
				<li className="list-group-item">Широта: {response.latitude}</li>
				<li className="list-group-item">Долгота: {response.longitude}</li>
			</ul>
		</section>
	)
}
export  default Success