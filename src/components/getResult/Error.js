import React from 'react'

function Error() {
	return (
		<section className='alert alert-danger'>
			<h3>Ошибка</h3>
			<p>Возможные причины:</p>
			<ol>
				<li>Вы ввели не корректное значение</li>
				<li>IP-адресс не действителен</li>
				<li>Превышен лимит запросов</li>
			</ol>
		</section>
	)
}
export default Error