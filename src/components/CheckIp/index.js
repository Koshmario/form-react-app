import React, {Component} from 'react'
import './CheckIp.scss'

class CheckIp extends Component {
	render() {
		const {handleChangeInput, getInfoForIP, ip} = this.props

		return (
			<section>
				<h1>Проверка IP-адреса</h1>
				<div className="card">
					<div className="card-body">
						<div className="form-group">
							<label htmlFor="IP">IP-адресс</label>
							<input
								value={ip}
								onChange={handleChangeInput}
								type="text"
								id="IP"
								className="form-control"
								placeholder="Введите значение"/>
							<small className="form-text text-muted">Пример адреса 8.8.8.8</small>
						</div>
						<button onClick={getInfoForIP} className="btn btn-primary">Проверить</button>
					</div>
				</div>
			</section>
		)
	}
}


export default CheckIp