import React from 'react'
import './History.scss'

function History(props) {
	let history = props.history.slice()
	history = history.reverse()
	let historyItems;

	if (history.length === 0) {
		historyItems = <p>История пуста</p>
	} else {
		historyItems = history.map(item =>
			<div key={item.id} className='history-items'>
				<p className='h3'>
					IP: {item.ip} &nbsp;
					({item.time})
				</p>
				<ul className="list-group">
					<li className="list-group-item">Страна: {item.country_rus}</li>
					<li className="list-group-item">Регион: {item.region_rus}</li>
					<li className="list-group-item">Город: {item.city_rus}</li>
					<li className="list-group-item">Широта: {item.latitude}</li>
					<li className="list-group-item">Долгота: {item.longitude}</li>
				</ul>
			</div>
		)
	}

	return (
		<section className='history-container'>
			<h2>История</h2>
			{historyItems}
		</section>

	)
}

export default History