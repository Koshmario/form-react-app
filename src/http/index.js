import axios from 'axios'

const api = axios.create({
	baseURL: 'https://api.2ip.ua/geo.json'
})

export default api